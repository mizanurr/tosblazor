﻿using TosBlazor.Configurations.Injector;

namespace TosBlazor.Core.Customers;

internal partial class DependencyResolver : IInjectServices
{
    public void Configure(IServiceCollection services)
    {
        services.AddTransient<ICustomerRepository, CustomerRepository>();
    }
}
