﻿namespace TosBlazor.Configurations.ResApis;
internal class ApiResponse<T>
{
    public string? Message { get; set; }
    public int Status { get; set; }
    public T? Data { get; set; } = default;
    public IDictionary<string, string[]> Errors { get; set; } = new Dictionary<string, string[]>(StringComparer.Ordinal);
}