﻿namespace TosBlazor.Core.Customers;

internal interface ICustomerRepository
{
    Task<IEnumerable<CustomerModel>?> GetAllAsync();
    Task<CustomerModel> GetByIdAsync(long id);
    Task PostAsync(CustomerModel customer);
    Task PutAsync(CustomerModel customer);
    Task DeleteAsync(long id);
}
