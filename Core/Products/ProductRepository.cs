﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using TosBlazor.Configurations.Constaints;
using TosBlazor.Configurations.ResApis;
using TosBlazor.Configurations.ResiliencePipelines;

namespace TosBlazor.Core.Products
{
    internal class ProductRepository : IProductRepository
    {
        private readonly HttpClient httpClient = new() { BaseAddress = new Uri(UrlConstants.BASE_API_URL) };
        public Task DeleteAsync(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ProductModel>?> GetAllAsync()
        {
            try
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await SecureStorage.GetAsync("accounttoken"));
                var result = await DefaultHttpPipeline
                    .Pipeline
                    .ExecuteAsync(
                      async _ => await httpClient.GetFromJsonAsync<ApiResponse<IEnumerable<ProductModel>>>(UrlConstants.PRODUCT, cancellationToken: _)
                     .ConfigureAwait(false));

                return result?.Data;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Task<ProductModel> GetAsync(long id)
        {
            throw new NotImplementedException();
        }

        public Task PostAsync(ProductModel product)
        {
            throw new NotImplementedException();
        }

        public Task PutAsync(ProductModel product)
        {
            throw new NotImplementedException();
        }
    }
}
