﻿using TosBlazor.Configurations.Constaints;
namespace TosBlazor.Configurations.ResApis;

internal  class HttpRestApi
{
    public static HttpClient Client => new(){ BaseAddress = new Uri(UrlConstants.BASE_API_URL) };
}
