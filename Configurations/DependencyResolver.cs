﻿using Microsoft.AspNetCore.Components.Authorization;
using TosBlazor.Configurations.Auth;
using TosBlazor.Configurations.ServicesInjector;

namespace TosBlazor.Configurations;
public static class DependencyResolver
{
    public static IServiceCollection AddConfigurationDependency(this IServiceCollection services)
    {
        services.AddScoped<CustomAuthenticationStateProvider>(); 
        services.AddScoped<AuthenticationStateProvider>(s => s.GetRequiredService<CustomAuthenticationStateProvider>());
        services.AddInjectServices();
        return services;
    }
}
