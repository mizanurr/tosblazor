﻿namespace TosBlazor.Core.Products;
internal sealed class ProductModel
{
    public string ProductCode { get; set; } = string.Empty;
    public string ProductName { get; set; } = string.Empty;
}
