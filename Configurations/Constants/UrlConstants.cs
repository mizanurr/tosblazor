﻿namespace TosBlazor.Configurations.Constaints;

internal  class UrlConstants
{
    public const string BASE_API_URL = "http://localhost:5086/api/";
    public const string BASE_API = "base_api";
   // public const string BASE_API_URL = "https://localhost:7261/api/";
    public const string LOGIN_USER = "account/login";
    public const string PRODUCT = "test/get-all-products";
    public const string CUSTOMER = "customer/get-all-customers";
}
