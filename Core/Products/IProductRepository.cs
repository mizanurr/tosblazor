﻿namespace TosBlazor.Core.Products;

internal interface IProductRepository
{
     Task<IEnumerable<ProductModel>?> GetAllAsync();
     Task<ProductModel> GetAsync(long id);
     Task PostAsync(ProductModel product);
     Task PutAsync(ProductModel product);
     Task DeleteAsync(long id);
}
