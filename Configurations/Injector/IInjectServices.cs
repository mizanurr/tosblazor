﻿namespace TosBlazor.Configurations.Injector;

internal interface IInjectServices
{
    void Configure(IServiceCollection services);
}
