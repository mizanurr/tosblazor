﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using TosBlazor.Configurations.Constaints;
using TosBlazor.Configurations.ResApis;
using TosBlazor.Configurations.ResiliencePipelines;

namespace TosBlazor.Core.Customers;

internal sealed class CustomerRepository : ICustomerRepository
{
    private static HttpClient Client => new () { BaseAddress = new Uri(UrlConstants.BASE_API_URL) };

    public Task DeleteAsync(long id)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<CustomerModel>?> GetAllAsync()
    {
        try
        {
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await SecureStorage.GetAsync("accounttoken"));
            var result = await DefaultHttpPipeline
                .Pipeline
                .ExecuteAsync(async _ => 
                    await Client.GetFromJsonAsync<ApiResponse<IEnumerable<CustomerModel>>>(UrlConstants.CUSTOMER, cancellationToken: _)
                .ConfigureAwait(false));
            return result?.Data;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public Task<CustomerModel> GetByIdAsync(long id)
    {
        throw new NotImplementedException();
    }

    public Task PostAsync(CustomerModel customer)
    {
        throw new NotImplementedException();
    }

    public Task PutAsync(CustomerModel customer)
    {
        throw new NotImplementedException();
    }
}
