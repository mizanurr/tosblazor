﻿namespace TosBlazor.ViewModels;

public class SecurityQuestionVM
{
    public bool IsDataUser { get; set; }
    public bool IsMNOAppActivated { get; set; }
    public bool HasFNFNumber { get; set; }
    public string? FnfNumber { get; set; }
    public bool MFSAccount { get; set; }
}
