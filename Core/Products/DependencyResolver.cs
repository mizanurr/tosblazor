﻿using TosBlazor.Configurations.Injector;

namespace TosBlazor.Core.Products;

internal partial class DependencyResolver: IInjectServices
{
    public void Configure(IServiceCollection services)
    {
        services.AddTransient<IProductRepository, ProductRepository>();
    }
}
