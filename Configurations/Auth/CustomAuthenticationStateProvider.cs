﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Net.Http.Json;
using System.Security.Claims;
using TosBlazor.ViewModels;
using TosBlazor.Configurations.Constaints;
using TosBlazor.Configurations.ResApis;

namespace TosBlazor.Configurations.Auth;
public class CustomAuthenticationStateProvider : AuthenticationStateProvider
{
    public Task Logout()
    {
        SecureStorage.Remove("accounttoken");
        NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        return Task.CompletedTask;
    }
    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        try
        {
            var userInfo = await SecureStorage.GetAsync("accounttoken");
            if (userInfo is not null)
            {
                var claims = new[] { new Claim(ClaimTypes.Name, "Sample User") };
                var identity = new ClaimsIdentity(claims, "Custom authentication");
                return new AuthenticationState(new ClaimsPrincipal(identity));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Request failed:" + ex.ToString());
        }
        return new AuthenticationState(new ClaimsPrincipal());
    }

    public async Task<bool> LoginAsync(LoginViewModel loginModel)
    {
        try
        {
            var response = await HttpRestApi.Client.PostAsJsonAsync(UrlConstants.LOGIN_USER, loginModel);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadFromJsonAsync<ApiResponse<LoginViewModel>>();
                await SecureStorage.SetAsync("accounttoken", content?.Data?.AccessToken ?? string.Empty);
                NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
                return true;
            }
            return false;
        }
        catch (Exception)
        {
            return false;
        }
    }
}
