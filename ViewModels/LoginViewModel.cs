﻿namespace TosBlazor.ViewModels;

public class LoginViewModel
{
    public  string Username { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public string AccessToken { get; set; } = string.Empty;
    public string RefreshToken { get; set; } = string.Empty;
    public UserViewModel? UserModel { get; set; }
    public bool LoginFailureHidden { get; set; } = true;
}
