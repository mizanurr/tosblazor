﻿using TosBlazor.Configurations.Injector;
namespace TosBlazor.Configurations.ServicesInjector;

internal static class InjectServices
{
    public static IServiceCollection AddInjectServices(this IServiceCollection services)
    {

        ServiceCollection injectors = new();
        injectors.Scan(scan => scan
                  .FromAssemblyOf<IInjectServices>()
                  .AddClasses(classes => classes.AssignableTo<IInjectServices>())
                  .AsImplementedInterfaces()
                  .WithTransientLifetime());

        var serviceProvider = injectors.BuildServiceProvider();
        serviceProvider.GetRequiredService<IEnumerable<IInjectServices>>()
                       .ToList()
                       .ForEach(x => x.Configure(services));
        return services;
    }
}
